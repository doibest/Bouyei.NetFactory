﻿using Bouyei.NetFactory.Protocols.Quic.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bouyei.NetFactory.Protocols.Quic.Frames
{
    public class DataBlockedFrame : Frame
    {
        public override byte Type => 0x14;
        public VariableInteger MaximumData { get; set; }

        public DataBlockedFrame()
        {

        }

        public DataBlockedFrame(UInt64 dataLimit)
        {
            MaximumData = dataLimit;
        }

        public override void Decode(ByteArray array)
        {
            byte type = array.ReadByte();
            MaximumData = array.ReadVariableInteger();
        }

        public override byte[] Encode()
        {
            var arry = MaximumData.ToByteArray();
            List<byte> result = new List<byte>(arry.Length + 1);
            result.Add(Type);
            result.AddRange(arry);

            return result.ToArray();
        }
    }
}
