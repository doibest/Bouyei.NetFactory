﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bouyei.NetFactory.Protocols.Quic.Streams
{
    public enum StreamState
    {
        Recv,
        SizeKnown,
        DataRecvd,
        DataRead,
        ResetRecvd
    }
}
