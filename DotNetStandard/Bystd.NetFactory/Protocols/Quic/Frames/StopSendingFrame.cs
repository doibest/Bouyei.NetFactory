﻿using Bystd.NetFactory.Protocols.Quic.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bystd.NetFactory.Protocols.Quic.Frames
{
    public class StopSendingFrame : Frame
    {
        public override byte Type => 0x05;

        public override void Decode(ByteArray array)
        {
            throw new NotImplementedException();
        }

        public override byte[] Encode()
        {
            throw new NotImplementedException();
        }
    }
}
