﻿using Bystd.NetFactory.Protocols.Quic.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bystd.NetFactory.Protocols.Quic.Frames
{
    public class MaxDataFrame : Frame
    {
        public override byte Type => 0x10;
        public VariableInteger MaximumData { get; set; }

        public override void Decode(ByteArray array)
        {
            array.ReadByte();
            MaximumData = array.ReadVariableInteger();
        }

        public override byte[] Encode()
        {
            var arry = MaximumData.ToByteArray();
            List<byte> result = new List<byte>(arry.Length+1);

            result.Add(Type);
            result.AddRange(arry);

            return result.ToArray();
        }
    }
}
