﻿using System;
using System.Linq;
using Bystd.NetFactory.Protocols.Quic.Utilities;
using Bystd.NetFactory.Protocols.Quic.Connections;
using Bystd.NetFactory.Protocols.Quic.Constants;
using Bystd.NetFactory.Protocols.Quic.Context;
using Bystd.NetFactory.Protocols.Quic.Events;
using Bystd.NetFactory.Protocols.Quic.Exceptions;
using Bystd.NetFactory.Protocols.Quic;
using Bystd.NetFactory.Protocols.Quic.Frames;
using Bystd.NetFactory.Protocols.Quic.PacketProcessing;
using Bystd.NetFactory.Protocols.Quic.Packets;
using Bystd.NetFactory.Protocols.Quic.Settings;
using Bystd.NetFactory.Protocols.Quic.InternalInfrastructure;
using System.Net.Sockets;

namespace Bystd.NetFactory.Protocols.Quic
{
    public class QuicServer : QuicTransport
    {
        private readonly Unpacker _unpacker;
        private readonly InitialPacketCreator _packetCreator;

        private PacketWireTransfer _pwt;

        private UdpClient _client;

        private int _port;
        private bool _started;

        public event ClientConnectedEvent OnClientConnected;

        public QuicServer(int port)
        {
            _started = false;
            _port = port;

            _unpacker = new Unpacker();
            _packetCreator = new InitialPacketCreator();
        }

        /// <summary>
        /// Starts the listener.
        /// </summary>
        public void Start()
        {
             _client = new UdpClient(_port);
             _started = true;
               _pwt = new PacketWireTransfer(_client, null);
           
            while (true)
            {
                Packets.Packet packet = _pwt.ReadPacket();
                if (packet is InitialPacket)
                {
                    QuicConnection connection = ProcessInitialPacket(packet, _pwt.LastTransferEndpoint());

                    OnClientConnected?.Invoke(connection);
                }

                if (packet is ShortHeaderPacket)
                {
                    ProcessShortHeaderPacket(packet);
                }
            }
        }

        /// <summary>
        /// Stops the listener.
        /// </summary>
        public void Close()
        {
            if (_started)
                _client.Close();
        }


        /// <summary>
        /// Processes incomming initial packet and creates or halts a connection.
        /// </summary>
        /// <param name="packet">Initial Packet</param>
        /// <param name="endPoint">Peer's endpoint</param>
        /// <returns></returns>
        private QuicConnection ProcessInitialPacket(Packets.Packet packet, System.Net.IPEndPoint endPoint)
        {
            QuicConnection result = null;
            UInt64 availableConnectionId;
            byte[] data;
            // Unsupported version. Version negotiation packet is sent only on initial connection. All other packets are dropped. (5.2.2 / 16th draft)
            if (packet.Version != QuicVersion.CurrentVersion || !QuicVersion.SupportedVersions.Contains(packet.Version))
            {
                VersionNegotiationPacket vnp = _packetCreator.CreateVersionNegotiationPacket();
                data = vnp.Encode();

                _client.Send(data, data.Length, endPoint);
                return null;
            }

            InitialPacket cast = packet as InitialPacket;
            InitialPacket ip = _packetCreator.CreateInitialPacket(0, cast.SourceConnectionId);

            // Protocol violation if the initial packet is smaller than the PMTU. (pt. 14 / 16th draft)
            if (cast.Encode().Length < QuicSettings.PMTU)
            {
                ip.AttachFrame(new ConnectionCloseFrame(ErrorCode.PROTOCOL_VIOLATION, 0x00, ErrorConstants.PMTUNotReached));
            }
            else if (ConnectionPool.AddConnection(new ConnectionData(new PacketWireTransfer(_client, endPoint), cast.SourceConnectionId, 0), out availableConnectionId) == true)
            {
                // Tell the peer the available connection id
                ip.SourceConnectionId = (byte)availableConnectionId;

                // We're including the maximum possible stream id during the connection handshake. (4.5 / 16th draft)
                ip.AttachFrame(new MaxStreamsFrame(QuicSettings.MaximumStreamId, StreamType.ServerBidirectional));

                // Set the return result
                result = ConnectionPool.Find(availableConnectionId);
            }
            else
            {
                // Not accepting connections. Send initial packet with CONNECTION_CLOSE frame.
                // TODO: Buffering. The server might buffer incomming 0-RTT packets in anticipation of late delivery InitialPacket.
                // Maximum buffer size should be set in QuicSettings.
                ip.AttachFrame(new ConnectionCloseFrame(ErrorCode.CONNECTION_REFUSED, 0x00, ErrorConstants.ServerTooBusy));
            }

            data = ip.Encode();
            int dataSent = _client.Send(data, data.Length, endPoint);
            if (dataSent > 0)
                return result;

            return null;
        }
    }
}
