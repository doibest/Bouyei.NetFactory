﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bouyei.NetFactoryCore.Protocols.Exceptions
{
    public class StreamException : Exception
    {
        public StreamException() { }

        public StreamException(string message) : base(message)
        {

        }
    }
}
