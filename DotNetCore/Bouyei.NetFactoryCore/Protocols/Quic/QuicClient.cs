﻿using System;
using System.Collections.Generic;
using Bouyei.NetFactoryCore.Protocols.Quic.Utilities;
using Bouyei.NetFactoryCore.Protocols.Quic.Connections;
using Bouyei.NetFactoryCore.Protocols.Quic.Context;
using Bouyei.NetFactoryCore.Protocols.Exceptions;
using Bouyei.NetFactoryCore.Protocols.Quic.Frames;
using Bouyei.NetFactoryCore.Protocols.Quic.PacketProcessing;
using Bouyei.NetFactoryCore.Protocols.Quic.Packets;
using Bouyei.NetFactoryCore.Protocols.Quic.Settings;
using Bouyei.NetFactoryCore.Protocols.Quic.InternalInfrastructure;
using System.Net;
using System.Net.Sockets;

namespace Bouyei.NetFactoryCore.Protocols.Quic
{
    /// <summary>
    /// Source of quic module project https://github.com/Vect0rZ/Quic.NET
    /// quic client
    /// </summary>
    public class QuicClient : QuicTransport
    {
        private IPEndPoint _peerIp;
        private UdpClient _client;

        private QuicConnection _connection;
        private InitialPacketCreator _packetCreator;

        private UInt64 _maximumStreams = QuicSettings.MaximumStreamId;
        private PacketWireTransfer _pwt;

        public QuicClient()
        {
            _client = new UdpClient();
            _packetCreator = new InitialPacketCreator();
        }

        /// <summary>
        /// Connect to a remote server.
        /// </summary>
        /// <param name="ip">Ip Address</param>
        /// <param name="port">Port</param>
        /// <returns></returns>
        public QuicConnection Connect(string ip, int port)
        {
            // Establish socket connection
            _peerIp = new IPEndPoint(IPAddress.Parse(ip), port);

            // Initialize packet reader
            _pwt = new PacketWireTransfer(_client, _peerIp);

            // Start initial protocol process
            InitialPacket connectionPacket = _packetCreator.CreateInitialPacket(0, 0);

            // Send the initial packet
            _pwt.SendPacket(connectionPacket);

            // Await response for sucessfull connection creation by the server
            InitialPacket packet = (InitialPacket)_pwt.ReadPacket();

            HandleInitialFrames(packet);
            EstablishConnection(packet.SourceConnectionId, packet.SourceConnectionId);

            return _connection;
        }

        /// <summary>
        /// Handles initial packet's frames. (In most cases protocol frames)
        /// </summary>
        /// <param name="packet"></param>
        private void HandleInitialFrames(Packets.Packet packet)
        {
            List<Frame> frames = packet.GetFrames();
            for (int i = frames.Count - 1; i > 0; i--)
            {
                Frame frame = frames[i];
                if (frame is ConnectionCloseFrame ccf)
                {
                    throw new QuicConnectivityException(ccf.ReasonPhrase);
                }

                if (frame is MaxStreamsFrame msf)
                {
                    _maximumStreams = msf.MaximumStreams.Value;
                }

                // Break out if the first Padding Frame has been reached
                if (frame is PaddingFrame)
                    break;
            }
        }

        /// <summary>
        /// Create a new connection
        /// </summary>
        /// <param name="connectionId"></param>
        /// <param name="peerConnectionId"></param>
        private void EstablishConnection(GranularInteger connectionId, GranularInteger peerConnectionId)
        {
            ConnectionData connection = new ConnectionData(_pwt, connectionId, peerConnectionId);
            _connection = new QuicConnection(connection);
        }
    }
}
