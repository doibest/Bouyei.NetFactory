﻿using Bouyei.NetFactoryCore.Protocols.Quic.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Bouyei.NetFactoryCore.Protocols.Quic.InternalInfrastructure
{
    internal class ConnectionData
    {
        public PacketWireTransfer PWT { get; set; }
        public GranularInteger ConnectionId { get; set; }
        public GranularInteger PeerConnectionId { get; set; }

        public SocketToken sToken { get; set; }

        public ConnectionData(PacketWireTransfer pwt, GranularInteger connectionId, GranularInteger peerConnnectionId)
        {
            PWT = pwt;
            ConnectionId = connectionId;
            PeerConnectionId = peerConnnectionId;
        }
        public ConnectionData(PacketWireTransfer pwt, SocketToken sToken, GranularInteger connectionId, GranularInteger peerConnnectionId)
        {
            this.sToken = sToken;
            PWT = pwt;
            ConnectionId = connectionId;
            PeerConnectionId = peerConnnectionId;
        }

        public ConnectionData(SocketToken sToken,GranularInteger connectionId, GranularInteger peerConnnectionId)
        {
            this.sToken = sToken;
            ConnectionId = connectionId;
            PeerConnectionId = peerConnnectionId;
        }
    }
}
