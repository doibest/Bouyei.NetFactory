﻿using Bouyei.NetFactoryCore.Protocols.Quic.Connections;
using Bouyei.NetFactoryCore.Protocols.Quic.Streams;

namespace Bouyei.NetFactoryCore.Protocols.Quic.Events
{
    public delegate void ClientConnectedEvent(QuicConnection connection);
    public delegate void StreamOpenedEvent(SocketToken sToken,QuicStream stream);
    public delegate void StreamDataReceivedEvent(SocketToken sToken, QuicStream stream, byte[] data);
    public delegate void ConnectionClosedEvent(QuicConnection connection);
}
